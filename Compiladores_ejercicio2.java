package compiladores_ejercicio2;
import java.util.Scanner;

public class Compiladores_ejercicio2 {

    
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int contador=0;
        
        System.out.println("Ingrese su palabra.");
        String frase = s.nextLine();
        
        for(int i=0;i<frase.length();i++){
            switch(frase.charAt(i)){
                case 'a': contador++; break;
                case 'e': contador++; break;
                case 'i': contador++; break;
                case 'o': contador++; break;
                case 'u': contador++; break;
            }
        }
        
        System.out.println("Número de vocales: "+contador);
    }
    
}